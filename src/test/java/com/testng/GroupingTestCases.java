package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class GroupingTestCases {
	
  @Test(groups={"Smoke Test"})
  public void createCustomer() {
	  
	  System.out.println("The Customer will Get Created");
  }
  @Test(groups={"Regression Test"})
  public void newCustomer()
  {
	  System.out.println("New Customer will Get Created");
  }
  
  @Test(groups={"Usability Testing"})
  public void modifyCustomer()
  {
	  System.out.println("The Customer will Get modified");
  }
  @Test(groups={"Smoke Test"})
  public void changeCustomer()
  {
	  System.out.println("The Customer will change");
  }
  
  @BeforeClass
  public void beforeClass()
  {
	  System.out.println("start database launch browser");
  }
  @AfterClass
  public void afterClass()
  {
	  System.out.println("close database connection close browser");
  }
}
