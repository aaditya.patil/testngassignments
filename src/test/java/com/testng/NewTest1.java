package com.testng;

import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewTest1 {
	@Test(priority=0)
	public void modifyCustomer() {

		System.out.println("Customer will get modified");
	}

	@Test(priority=2)
	public void createCustomer() {

		System.out.println("Customer will get created");
	}

	@Test(priority=1)
	public void newCustomer()
	{
		System.out.println("new customer will created");
	}

	@BeforeMethod
	public void beforeCustomer()
	{
		System.out.println("Verifying the customer");
	}

	@AfterMethod
	public void afterCustomer()
	{
		System.out.println("all the transactions are done");
	}

	@org.testng.annotations.BeforeClass
	public void beforeclassMethod()
	{
		System.out.println("Start database connection, launch browser");
	}

	@org.testng.annotations.AfterClass
	public void afterclassMethod()
	{
		System.out.println("close database connection, close browser");

	}
}


