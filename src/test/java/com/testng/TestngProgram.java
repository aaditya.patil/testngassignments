package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestngProgram {
	
	WebDriver driver;
  
	@BeforeClass
	public void launchBrowser()
	{
		System.out.println("Before launch browser");
    }
	
	 @BeforeMethod
	 public void beforeMethod()
	 {
         System.out.println("execute the before method");
	 }
	 
	 @Test
	 public void loginUser()
	 {
		 driver = new ChromeDriver();
		 
         driver.get("https://demowebshop.tricentis.com/login");
		 
		 driver.manage().window().maximize();
		 
		 driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		 
         driver.findElement(By.id("Email")).sendKeys("aadityapatil18@gmail.com");
		 
		 driver.findElement(By.id("Password")).sendKeys("Aaditya12");
		 
		 driver.findElement(By.xpath("//input[@value='Log in']")).click();
	 }
	 @AfterMethod
	 public void afterMethod()
	 {
		 driver.close();
	 }
	 
	 @AfterClass
	 public void afterClass()
	 {
		 System.out.println("program execute successfully");
	 }
}
