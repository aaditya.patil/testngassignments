package com.testng;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrameClass {
	
	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.selenium.dev/selenium/docs/api/java/index.html?overview-summary.html");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		driver.switchTo().frame("packageListFrame");
		
		driver.findElement(By.partialLinkText("org.openqa.selenium")).click();
		
		driver.navigate().refresh();
		
		Thread.sleep(2000);
		
		driver.switchTo().frame("packageListFrame");
		
		driver.findElement(By.partialLinkText("org.openqa.selenium.bidi")).click();
		
		
		
		
	}

}
